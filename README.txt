- Panels should be upgraded to latest dev snapshot
- Set menu localizatin option to option 1 admin/structure/menu/manage/menu-erpal-menu/edit
- apply this patch i18nmenu module :
	- https://www.drupal.org/node/2339823
	- https://www.drupal.org/node/1897696#comment-8866097
- Translation source should be refreseshed
- here http://pm.practicalidea.com/en/admin/structure/taxonomy taxonomy localize term options should be enabled for all vocabs
- now time to import the po

- add new role translator to give access for translation

TODO : 
- tasks semantec categories are not translatable
- install calendar systems module

WARNING : 
- Note that content selection module cause incompatibitly with comment alter or comment modules making comments disppear!